import 'dart:io';

int m = 0,flag=0;
void primeNumber(int a) {
  m = a ~/ 2;
  for (int i = 2; i <= m; i++) {
    if (a % i == 0) {
      print('$a is not a prime number');
      flag=1;
      break;
    }
  }
  if(flag==0){
    print('$a is a prime number');
  }
}

void main() {
  print("Input Integer number : ");
  int? n = int.parse(stdin.readLineSync()!);
  primeNumber(n);
}

